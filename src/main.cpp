#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <vector>
#include <algorithm>
#include <functional>
#include <omp.h>
#include <math.h>

namespace py = pybind11;


/* ----------------------------------
 * Radiological Depth Calculation
   ----------------------------------*/
py::array_t<double> radDepth(py::array_t<double> x, py::array_t<double> y, py::array_t<double> z, \
                             py::array_t<double> ct, py::array_t<int> bdy , \
                             py::array_t<double> b_cen, py::array_t<double> p_x, py::array_t<double> p_y, \
                             py::array_t<double> p_z, double sad, double mu_not){

    // ---- Get Buffer info from input numpy arrays -------
    py::buffer_info xceninfo = x.request();
    auto xcen = static_cast<double *>(xceninfo.ptr);

    py::buffer_info yceninfo = y.request();
    auto ycen = static_cast<double *>(yceninfo.ptr);

    py::buffer_info zceninfo = z.request();
    auto zcen = static_cast<double *>(zceninfo.ptr);

    py::buffer_info CTinfo = ct.request();
    auto CT = static_cast<double *>(CTinfo.ptr);

    py::buffer_info Bodyinfo = bdy.request();
    auto Body = static_cast<int *>(Bodyinfo.ptr);

    py::buffer_info BeamCeninfo = b_cen.request();
    auto beam_cen = static_cast<double *>(BeamCeninfo.ptr);

    py::buffer_info pxinfo = p_x.request();
    double* px = static_cast<double *>(pxinfo.ptr);

    py::buffer_info pyinfo = p_y.request();
    double* py = static_cast<double *>(pyinfo.ptr);

    py::buffer_info pzinfo = p_z.request();
    double* pz = static_cast<double *>(pzinfo.ptr);

    // ----- Define defualt parameters ------
    double x1 = beam_cen[0];
    double y1 = beam_cen[1];
    double z1 = beam_cen[2];
    double dx = xcen[1] - xcen[0];
    double dy = ycen[1] - ycen[0];
    double dz = zcen[1] - zcen[0];
    ssize_t Nx = xceninfo.shape[0];
    ssize_t Ny = yceninfo.shape[0];
    ssize_t Nz = zceninfo.shape[0];
    unsigned int N = pxinfo.shape[0];
    int xend = Nx - 1;
    int yend = Ny - 1;
    int zend = Nz - 1;
    int Rows = Bodyinfo.shape[0];
    int Cols = Bodyinfo.shape[1];
    int Slices = Bodyinfo.shape[2];

    // ---- Init rad depths vector ---------
    std::vector<double> drads(N, 0.0);

    // ---- calc radiological depth for each point ----
#pragma omp parallel for firstprivate(xcen, ycen, zcen, Body, CT, px, py, pz)
    for(int it=0; it < N; it++ ) {
        // ---- termination point ---
        py::print(it);
        double x2 = px[it]; double y2 = py[it]; double z2 = pz[it];
        // ---- Create alpha sets ----
        std::vector<double> ax(Nx, 0.0);
        std::vector<double> ay(Ny, 0.0);
        std::vector<double> az(Nz, 0.0);
        if(x2 != x1){
            for(int ai=0; ai< Nx; ai++)
            {
                ax[ai] = (xcen[ai]-x1)/(x2-x1);
            }
        }
        // ---- Create alpha sets ----
        if(y2 != y1){
            for(int ai=0; ai< Ny; ai++)
            {
                ay[ai] = (ycen[ai]-y1)/(y2-y1);
            }
        }
        // ---- Create alpha sets ----
        if(z2 != z1){
            for(int ai=0; ai< Nz; ai++)
            {
                az[ai] = (zcen[ai]-z1)/(z2-z1);
            }
        }

        // ------ find amin ----------
        double minax = *std::min_element(std::begin(ax), std::end(ax));
        double minay = *std::min_element(std::begin(ay), std::end(ay));
        double minaz = *std::min_element(std::begin(az), std::end(az));
        double amin = std::max(0.0, minax); amin = std::max(amin, minay); amin = std::max(amin, minaz);

        // ------ find amax -----------
        double maxax = *std::max_element(std::begin(ax), std::end(ax));
        double maxay = *std::max_element(std::begin(ay), std::end(ay));
        double maxaz = *std::max_element(std::begin(az), std::end(az));
        double amax = std::min(1.0, maxax); amax = std::min(amax, maxay); amax = std::min(amax, maxaz);

        // ------ find {i,j,k}min and {i,j,k}max -------
        int imin = 0, imax = 0, jmin = 0, jmax = 0, kmin = 0, kmax = 0;
        if(x2-x1 > 0){
            imin = Nx - (xcen[xend]-amin*(x2-x1)-x1)/dx;
            imax = 1 + (x1+amax*(x2-x1)-xcen[0])/dx;
        }
        else if(x2-x1 < 0){
            imin = Nx - (xcen[xend]-amax*(x2-x1)-x1)/dx;
            imax = 1 + (x1+amin*(x2-x1)-xcen[0])/dx;
        }

        if(y2-y1 > 0){
            jmin = Ny - (ycen[yend]-amin*(y2-y1)-y1)/dy;
            jmax = 1 + (y1+amax*(y2-y1)-ycen[0])/dy;
        }
        else if(y2-y1 < 0){
            jmin = Ny - (ycen[yend]-amax*(y2-y1)-y1)/dy;
            jmax = 1 + (y1+amin*(y2-y1)-ycen[0])/dy;
        }

        if(z2-z1 > 0){
            kmin = Nz - (zcen[zend]-amin*(z2-z1)-z1)/dz;
            kmax = 1 + (z1+amax*(z2-z1)-zcen[0])/dz;
        }
        else if(z2-z1 < 0){
            kmin = Nz - (zcen[zend]-amax*(z2-z1)-z1)/dz;
            kmax = 1 + (z1+amin*(z2-z1)-zcen[0])/dz;
        }

        // --- slice alpha sets ----
        std::vector<double> ax2(ax.begin()+imin, ax.begin()+imax);
        std::vector<double> ay2(ay.begin()+jmin, ay.begin()+jmax);
        std::vector<double> az2(az.begin()+kmin, az.begin()+kmax);


        // --- concantenate into one ---
        std::vector<double> a1(ax2.size()+ay2.size()+ax2.size()+2);
        //a.insert(a.begin(),amin);
        a1[0] = amin;
        a1.insert(a1.end(),amax);
        a1.insert(a1.begin()+1,ax2.begin(),ax2.end());
        a1.insert(a1.begin()+1+ax2.size(),ay2.begin(),ay2.end());
        a1.insert(a1.begin()+1+ax2.size()+ay2.size(),az2.begin(),az2.end());


        // --- sort a ---
        std::sort(a1.begin(),a1.end());

        // --- get unique elements ---
        std::vector<double>::iterator iter;
        iter = std::unique(a1.begin(),a1.end());
        a1.resize(std::distance(a1.begin(),iter));

        std::vector<double> a(a1.size()-1);
        for(int ia=0;ia<a.size();ia++){
            a[ia] = a1[ia+1];
        }


        // --- total distance ---
        double d12 = ((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));
        d12 = sqrt(d12);

        // --- create amid vector ---
        int aend = a.size()-1;
        std::vector<double> an(a.begin(),a.end()-1);
        std::vector<double> anp1(a.begin()+1,a.end());
        std::vector<double> amid(an.size(),0.0);
        // add an and anp1
        std::transform(an.begin(),an.end(),anp1.begin(),amid.begin(), std::plus<double>());

        //divide amid by 2
        for(int jj=0; jj<amid.size(); jj++){ amid[jj] = amid[jj]/2;}


        // --- create i,j,k sets ---
        std::vector<int> i(amid.size());
        std::vector<int> j(amid.size());
        std::vector<int> k(amid.size());

        for(int kk=0;kk<amid.size();kk++){
            i[kk] = floor((x1 + amid[kk]*(x2-x1)-xcen[0])/dx)-1;
            j[kk] = floor((y1 + amid[kk]*(y2-y1)-ycen[0])/dy)-1;
            k[kk] = floor((z1 + amid[kk]*(z2-z1)-zcen[0])/dz)-1;
        }


        // --- find entrance ---
        int start = 0;
        int lindex = 0;
        int lindex2 = 0;
        for(int p = 0; p < amid.size(); p++){
            if(i[p] < Nx && j[p] < Ny && k[p] < Nz){
                if(i[p] > 0 && j[p] > 0 && k[p] > 0){
                    lindex = j[p]*Cols*Slices + i[p]*Slices+k[p];
                    if(Body[lindex] != 0){
                        start = p;
                        break;
                    }
                }
            }
        }

        double drad = 0;
        for(int iii=start;iii<amid.size()-1;iii++){
            lindex = j[iii]*Cols*Slices + i[iii]*Slices + k[iii];
            drad = drad + (d12*(amid[iii+1]-amid[iii])*CT[lindex])/mu_not;
        }

        drads[it] = drad;
    }
    // Note to initialize a py::array_t<double>
    // double result[2][3][4];
    // py::array_t<double>({2,3,4},&result[0]);
    return py::array_t<double>({drads.size()}, &drads[0]);

}



PYBIND11_MODULE(mppack, m) {
    m.doc() = "medical physics python module"; // optional module docstring
    m.def("radDepth", &radDepth);
}
